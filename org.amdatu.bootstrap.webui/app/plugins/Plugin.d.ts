/// <reference path="./CommandDescription.d.ts" />
interface Plugin {
    pluginName : String;
    commandDescription : CommandDescription[];
}

