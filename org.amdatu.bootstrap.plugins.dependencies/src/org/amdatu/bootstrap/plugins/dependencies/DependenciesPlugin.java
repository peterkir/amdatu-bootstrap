/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.dependencies;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.DynamicSelect;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.EnumValue;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.version.Version;
import aQute.bnd.version.VersionRange;

@Component
public class DependenciesPlugin implements BootstrapPlugin{
	
	private final Collector<InstallResult, ?, Optional<InstallResult>> INSTALLRESULT_REDUCER 
			= Collectors.reducing((a,b) -> InstallResult.builder().addResult(a).addResult(b).build());
	
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile DependencyBuilder m_dependencies;
	
	interface ListDependenciesArgs extends Parameters {
		@Description("Filters to ignore bundles, comma separated")
		String filter();
	}
	
	@Command(scope=Scope.WORKSPACE)
	public List<String> listBuild(ListDependenciesArgs args, Prompt prompt) {
		
		Stream<Dependency> dependencies;
		
		if(m_navigator.getCurrentScope() == Scope.PROJECT) {
			dependencies = m_dependencies.listProjectBuildDependencies().stream();
		} else {
			dependencies = m_dependencies.listWorkspaceBuildDependencies().stream();
		}
		
		List<String> filters = createBsnFilters(args);
		
		return dependencies
				.filter(d -> !matchesFilters(d.getBsn(), filters))
				.collect(Collectors.groupingBy(Dependency::getBsn))
				.entrySet()
				.stream()
				.map(this::dependenciesToString)
				.sorted()
				.collect(Collectors.toList());
	}

	private List<String> createBsnFilters(ListDependenciesArgs args) {
		List<String> filters = new ArrayList<>();
		if(args.filter() != null && args.filter().length() > 0) {
			String[] split = args.filter().split(",");
			Arrays.stream(split).map(String::trim).forEach(filters::add);
		}
		return filters;
	}
	
	private boolean matchesFilters(String bsn, List<String> filters) {
		Optional<String> matches = filters.stream().filter(f -> bsn.matches(f)).findAny();
		return matches.isPresent();
	}
	
	interface ListRunDependenciesArgs extends ListDependenciesArgs {
		@RunConfig
		File runFile();
	}
	
	@Command(scope=Scope.WORKSPACE)
	public List<String> listRun(ListRunDependenciesArgs args, Prompt prompt) {
		
		Stream<Dependency> dependencies = m_dependencies.listRunDependencies(args.runFile().toPath()).stream();
		List<String> filters = createBsnFilters(args);
		return dependencies
				.filter(d -> !matchesFilters(d.getBsn(), filters))
				.collect(Collectors.groupingBy(Dependency::getBsn))
				.entrySet()
				.stream()
				.map(this::dependenciesToString)
				.sorted()
				.collect(Collectors.toList());
	}
	
	interface ListVersionsArgs extends Parameters {
		@Required
		@Description("Bundle Symbolic Name of the bundle to find versions for")
		String bsn();
	}
	
	@Command(scope=Scope.WORKSPACE)
	public String availableVersions(ListVersionsArgs args) {
		StringBuilder b = new StringBuilder();
		b.append("Versions found for ").append(args.bsn()).append(": ");
		
		Set<Version> versions = m_dependencies.listAvailableVersions(args.bsn());
		versions.stream()
				.map(Version::toString)
				.forEach(v -> b.append(v).append(", "));
		
		if(!versions.isEmpty()) {
			b.delete(b.length() - 2, b.length());
		}
		return b.toString();
	}
	
	@Command(scope=Scope.WORKSPACE)
	public InstallResult updateRun(ListRunDependenciesArgs args, Prompt prompt) {
		List<String> filters = createBsnFilters(args);
		Map<String, NewAndOldVersion> upgradables = m_dependencies.listRunDependencies(args.runFile().toPath()).stream()
			.filter(d -> !matchesFilters(d.getBsn(), filters))
			.map(this::dependencyToNewAndOld)
			.filter(v -> v.getNew().isPresent())
			.filter(v -> !v.getOld().isPresent() || v.getNew().get().compareTo(v.getOld().get().getHigh()) > 0)
			.collect(Collectors.toMap(v -> v.getBsn(), Function.identity()));
		
		Map<String, String> promptValues = upgradables.values().stream()
			.collect(Collectors.toMap(v -> v.getBsn(), v->v.getNew().get().toString()));
		
		Map<String, Boolean> selected = prompt.askMultipleBoolean("Select bundles to upgrade", promptValues, true);
		
		Optional<InstallResult> result = selected.keySet().stream()
			.filter(k -> selected.get(k) == true)
			.map(s -> upgradables.get(s))
			.map(d -> m_dependencies.updateRunDependency(d.getBsn(), d.getNew().get().toString(), args.runFile().toPath()))
			.collect(INSTALLRESULT_REDUCER);
		
		if(result.isPresent()) {
			return result.get();
		} else {
			return InstallResult.empty();
		}
	}
	
	interface UpdateVersionsArgs extends Parameters {
		@DynamicSelect("listBuildVersions")
		String bundle();
	}
	
	@Command(scope=Scope.WORKSPACE)
	public InstallResult updateBuild(UpdateVersionsArgs args, Prompt prompt) {
		
		List<String> versions = new ArrayList<>();
		versions.add("latest");
		
		List<String> availableVersions = m_dependencies.listAvailableVersions(args.bundle()).stream()
				.map(v -> v.toString())
				.collect(Collectors.toList());
		
		versions.addAll(availableVersions);
		
		String version = prompt.askChoice("Upgrade to which version?", 0, versions);
		
		if(m_navigator.getCurrentScope() == Scope.WORKSPACE) {
			Optional<InstallResult> result = m_navigator.getProjectsInWorkspace().stream()
				.map(p -> p.getFile("bnd.bnd").toPath())
				.map(p -> m_dependencies.updateBuildDependency(args.bundle(), version, p))
				.collect(INSTALLRESULT_REDUCER);
			
			if(result.isPresent()) {
				return result.get();
			} else {
				return InstallResult.empty();
			}
			
		} else {
			return m_dependencies.updateBuildDependency(args.bundle(), version, m_navigator.getBndFile());
		}
	}
	
	public List<EnumValue> listBuildVersions() {
		Stream<Dependency> dependencies;
		
		if(m_navigator.getCurrentScope() == Scope.PROJECT) {
			dependencies = m_dependencies.listProjectBuildDependencies().stream();
			
		} else {
			dependencies = m_dependencies.listWorkspaceBuildDependencies().stream();
		}
		
		return dependencies
				.map(d -> new EnumValue(d.getBsn(), d.getBsn() + " " + d.getVersion()))
				.collect(Collectors.toList());
	}
	
	private NewAndOldVersion dependencyToNewAndOld(Dependency d) {
		
		VersionRange oldVersion;
		
		if(d.getVersion() == null || d.getVersion().equals("latest")) {
			oldVersion = null;
		} else {
			oldVersion = new VersionRange(d.getVersion());
		}
		
		Optional<VersionRange> old = Optional.ofNullable(oldVersion);
		
		return new NewAndOldVersion(d.getBsn(), old, m_dependencies.listHighestVersion(d.getBsn()));
	}
	
	class NewAndOldVersion {
		private String m_bsn;
		private Optional<VersionRange> m_old;
		private Optional<Version> m_new;
		
		public NewAndOldVersion(String bsn, Optional<VersionRange> old, Optional<Version> new1) {
			m_bsn = bsn;
			m_old = old;
			m_new = new1;
		}
		public String getBsn() {
			return m_bsn;
		}

		public Optional<VersionRange> getOld() {
			return m_old;
		}

		public Optional<Version> getNew() {
			return m_new;
		}
	}
	
	private String dependenciesToString(Map.Entry<String, List<Dependency>> dependencies) {
		StringBuilder b = new StringBuilder(dependencies.getKey())
			.append(": ");
		
		dependencies.getValue().forEach(d -> b.append(d.getVersion()).append(", "));
		
		b.delete(b.length() - 2, b.length());
		
		return b.toString();
	}
	
	
	
	@Override
	public String getName() {
		return "dependencies";
	}
}
