/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.util.Collections;
import java.util.List;

import aQute.bnd.annotation.ProviderType;

@ProviderType
public class CommandArgumentDescription {
	private String m_name;
	private String m_type;
	private String m_description;
	private boolean m_required;
	private List<EnumValue> m_options;
	private String m_callback;
	private boolean m_store;

	public CommandArgumentDescription() {
	}
	
	public CommandArgumentDescription(String name, String description, boolean required, boolean store, String callback) {
		m_name = name;
		m_type = "enum";
		m_description = description;
		m_required = required;
		m_options = Collections.emptyList();
		m_callback = callback;
		m_store = store;
	}
	
	public CommandArgumentDescription(String name, String type, String description, boolean required, boolean store) {
		m_name = name;
		m_type = type;
		m_description = description;
		m_required = required;
		m_options = Collections.emptyList();
		m_callback = null;
		m_store = store;
	}
	
	public CommandArgumentDescription(String name, String description, boolean required, boolean store, List<EnumValue> options) {
		m_name = name;
		m_type = "enum";
		m_description = description;
		m_required = required;
		m_options = options;
		m_callback = null;
		m_store = store;
	}

	public String getName() {
		return m_name;
	}

	public String getType() {
		return m_type;
	}

	public String getDescription() {
		return m_description;
	}

	public boolean isRequired() {
		return m_required;
	}

	public List<EnumValue> getOptions() {
		return m_options;
	}

	public String getCallback() {
		return m_callback;
	}

	public void setName(String name) {
		m_name = name;
	}

	public void setType(String type) {
		m_type = type;
	}

	public void setDescription(String description) {
		m_description = description;
	}

	public void setRequired(boolean required) {
		m_required = required;
	}

	public void setOptions(List<EnumValue> options) {
		m_options = options;
	}

	public void setCallback(String callback) {
		m_callback = callback;
	}

	public boolean isStore() {
		return m_store;
	}

	public void setStore(boolean store) {
		m_store = store;
	}
}
