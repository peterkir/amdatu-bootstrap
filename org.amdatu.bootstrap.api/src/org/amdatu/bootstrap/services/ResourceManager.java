/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import aQute.bnd.annotation.ProviderType;

/**
 * Service that provides high level file and file system operations. 
 */
@ProviderType
public interface ResourceManager {
	void writeFile(Path path, byte[] contents);
	/**
	 * Unzips a file to a folder
	 * @param fileToUnzip the file to unzip
	 * @param toLocation a location to store the contents of the zip file. This must be a folder.
	 */
	void unzipFile(File fileToUnzip, File toLocation);

	/**
	 * Removes a file or directory, if the directory contains files all files will be deleted as well.
	 * @param fileToDelete File or directory to delete.
	 * @throws IOException  Error while deleting the file or directory. 
	 */
	void delete(File fileToDelete) throws IOException;
	
	/**
	 * Search for files containing the input string.
	 * @param folderToStartSearching The directory to start the search
	 * @param what The string to search for
	 * @return A list of files containing the search string
	 */
    List<File> findFiles(File folderToStartSearching, String what);
}
