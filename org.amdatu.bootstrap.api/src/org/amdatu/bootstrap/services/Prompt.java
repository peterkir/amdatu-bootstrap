/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.services;

import java.util.List;
import java.util.Map;

import org.amdatu.bootstrap.command.FullyQualifiedName;

import aQute.bnd.annotation.ProviderType;

/**
 * Service to interactively prompt the user for input. Avoid over-using this service; statically defining input arguments 
 * gives a nicer user experience when possible.
 */
@ProviderType
public interface Prompt {
	boolean askBoolean(String message, boolean defaultChoice);

	String askString(String message);

	String askString(String message, String defaultString);

	<T> T askChoice(String message, int defaultOption, List<T> options);

	int askChoiceAsIndex(String message, int defaultOption,List<? extends Object> options);

	FullyQualifiedName askComponentName();

	FullyQualifiedName askComponentName(String message);

	FullyQualifiedName askComponentName(String message, String defaultName);

	Map<String, Boolean> askMultipleBoolean(String message, Map<String, String> questions, boolean defaultChoice); 
	
	Map<String, Boolean> askMultipleBoolean(String message, List<String> questions, boolean defaultChoice);
	
	void println(String message);
	
	void printf(String message, Object... args);
	
	void printStatus(int current, int max);
}
