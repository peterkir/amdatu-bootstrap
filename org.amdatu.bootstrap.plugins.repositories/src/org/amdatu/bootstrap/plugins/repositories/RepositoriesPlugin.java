/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.repositories;

import java.io.File;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Select;
import org.amdatu.bootstrap.command.SelectOption;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.Prompt;
import org.amdatu.bootstrap.services.repositories.DependencyFilter;
import org.amdatu.bootstrap.services.repositories.DependencyType;
import org.amdatu.bootstrap.services.repositories.RepositoriesService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class RepositoriesPlugin implements BootstrapPlugin{
	@ServiceDependency 
	private RepositoriesService m_repositoriesService;
	
	interface MaterializeParams extends Parameters {
		@RunConfig
		File bndFile();
		
		File outputDir();
		
		@Select(value={
				@SelectOption(description="Runtime dependencies", value="RUN"), 
				@SelectOption(description="Build dependencies", value="BUILD"),
				@SelectOption(description="Run and build dependencies", value="ALL")})
		String dependenciesType();
		
		@Select(value={
				@SelectOption(description="No filter", value="NONE"), 
				@SelectOption(description="Include only non-workspace bundles", value="NON_WORKSPACE_ONLY"),
				@SelectOption(description="Include only workspace bundles", value="WORKSPACE_ONLY")})
		String dependenciesFilter();
	}
	
	@Command
	public void materialize(MaterializeParams args, Prompt prompt) {
		DependencyType dependencyType = DependencyType.valueOf(args.dependenciesType());
		DependencyFilter dependencyFilter = DependencyFilter.valueOf(args.dependenciesFilter());
		
		m_repositoriesService.materialize(args.bndFile(), args.outputDir().toPath(), dependencyType, dependencyFilter);
	}

	@Override
	public String getName() {
		return "repositories";
	}
}
