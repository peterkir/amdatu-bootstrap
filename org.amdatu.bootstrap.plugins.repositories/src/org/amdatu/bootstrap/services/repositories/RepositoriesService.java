package org.amdatu.bootstrap.services.repositories;

import java.io.File;
import java.nio.file.Path;

public interface RepositoriesService {
	void materialize(File bndFile, Path target, DependencyType dependencyTypes);
	void materialize(File bndFile, Path target, DependencyType dependencyTypes, DependencyFilter filter);
}
