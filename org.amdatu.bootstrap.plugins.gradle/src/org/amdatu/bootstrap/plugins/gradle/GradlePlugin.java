/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.gradle;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.gradle.GradleService;
import org.amdatu.bootstrap.services.Prompt;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class GradlePlugin implements BootstrapPlugin{
	
	@ServiceDependency
	private volatile GradleService m_gradleService;
	
	interface GradleArgs extends Parameters {
		String[] args();
	}
	
	@Command(scope=Scope.WORKSPACE)
	public void build(GradleArgs args, Prompt prompt) {
		m_gradleService.build(prompt, args.args());
	}

	@Override
	public String getName() {
		return "gradle";
	}
}
