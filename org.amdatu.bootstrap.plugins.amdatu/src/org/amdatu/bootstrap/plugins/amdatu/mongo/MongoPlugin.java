/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.amdatu.mongo;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.InstallResult.Builder;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;


@Component
public class MongoPlugin implements BootstrapPlugin{
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	interface MongoInstallParameters extends Parameters {
		@Description("Install MongoJack")
		boolean mongojack();
		
		@Description("Install Jongo")
		boolean jongo();
	}

	@Command(scope=Scope.PROJECT)
	public InstallResult install(MongoInstallParameters params) {
		List<Dependency> dependencies = Dependency.fromStrings("org.mongodb.mongo-java-driver", "org.amdatu.mongo");

		if(params.mongojack()) {
			installMongoJack(dependencies);
		}
		
		if(params.jongo()) {
			installJongo(dependencies);
		}
		
		return m_dependencyBuilder.addDependencies(dependencies);
	}

	private void installJongo(List<Dependency> dependencies) {
		dependencies.addAll(Dependency.fromStrings(
				"com.fasterxml.jackson.core.jackson-core;version='[2.3.1,2.3.1]'",
				"com.fasterxml.jackson.core.jackson-databind;version='[2.3.1,2.3.1]'",
				"com.fasterxml.jackson.core.jackson-annotations;version='[2.3.0,2.3.0]'",
				"org.jongo", 
				"de.undercouch.bson4jackson",
				"javax.persistence"));
	}

	private void installMongoJack(List<Dependency> dependencies) {
		dependencies.addAll(Dependency.fromStrings(
				"com.fasterxml.jackson.core.jackson-core;version='[2.3.1,2.3.1]'",
				"com.fasterxml.jackson.core.jackson-databind;version='[2.3.1,2.3.1]'",
				"com.fasterxml.jackson.core.jackson-annotations;version='[2.3.0,2.3.0]'", 
				"org.mongojack;version=2.1.0.SNAPSHOT",
				"de.undercouch.bson4jackson",
				"javax.persistence"));
	}
	
	interface MongoRunConfigParameters extends Parameters {
		@Description("Location of run configuration")
		@RunConfig
		File runFile();
		
		@Description("Install MongoJack")
		boolean mongojack();
		
		@Description("Install Jongo")
		boolean jongo();
	}
	
	@Command(scope=Scope.PROJECT)
	public InstallResult run(MongoRunConfigParameters params) {
		Builder builder = InstallResult.builder();
		
		List<Dependency> dependencies = Dependency.fromStrings(
				"org.mongodb.mongo-java-driver", 
				"org.amdatu.mongo",
				"org.apache.felix.configadmin");
		
		if(params.mongojack()) {
			installMongoJack(dependencies);
		}
		
		if(params.jongo()) {
			installJongo(dependencies);
		}
		
		Path path = m_navigator.getCurrentDir().resolve(params.runFile().toPath());
		
		builder.addResult(m_dependencyBuilder.addRunDependency(dependencies, path));
		builder.addResult(m_dmService.addRunDependencies(path));
		
		return builder.build();
	}

	@Override
	public String getName() {
		return "mongo";
	}
}
