/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.project;

import org.amdatu.bootstrap.template.BundleTemplateProvider;
import org.amdatu.bootstrap.template.TemplateProvider;
import org.apache.felix.dm.annotation.api.BundleDependency;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.framework.Bundle;

@Component(provides=TemplateProvider.class, properties=@Property(name="type", value="project"))
public class ProjectBundleTemplateProvider extends BundleTemplateProvider {

	public static final String PROJECT_TEMPLATE_HEADER = "X-Bootstrap-ProjectTemplate";

	public static final String PROJECT_TEMPLATE_BUNDLE_FILTER = "(" + PROJECT_TEMPLATE_HEADER + "=*)";

	
	public ProjectBundleTemplateProvider() {
		super(PROJECT_TEMPLATE_HEADER);
	}
	
	@Override
	@BundleDependency(filter = PROJECT_TEMPLATE_BUNDLE_FILTER, removed="bundleRemoved")
	protected void bundleAdded(Bundle bundle) {
		super.bundleAdded(bundle);
	}
	
	@Override
	protected void bundleRemoved(Bundle bundle) {
		super.bundleRemoved(bundle);
	}
}
