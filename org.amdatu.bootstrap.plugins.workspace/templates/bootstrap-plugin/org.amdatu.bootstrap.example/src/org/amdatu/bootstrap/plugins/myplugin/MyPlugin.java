package org.amdatu.bootstrap.plugins.myplugin;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.Prompt;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class MyPlugin implements BootstrapPlugin{
	interface MyPluginArgs extends Parameters {
		@Description("Your name")
		String name();
	}
	
	@Command
	public void demo(MyPluginArgs args, Prompt prompt) {
		prompt.printf("Hello, %s", args.name());
	}

	@Override
	public String getName() {
		return "myplugin";
	}
}