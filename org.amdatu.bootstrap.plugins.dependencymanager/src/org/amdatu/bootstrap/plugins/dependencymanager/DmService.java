/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.dependencymanager;

import java.nio.file.Path;

import org.amdatu.bootstrap.command.FullyQualifiedName;
import org.amdatu.bootstrap.command.InstallResult;

public interface DmService {
	InstallResult installAnnotationProcessor();
	InstallResult addDependencies();
	void createActivator(FullyQualifiedName fqn, FullyQualifiedName interfaceName);
	InstallResult addRunDependencies(Path runFilePath);
}
