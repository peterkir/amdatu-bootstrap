/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.ws;

import java.util.Map;

public class PromptMultiQuestion<T> {
	private String m_id;
	private String m_type;
	private String m_description;
	private T m_defaultValue;
	private Map<String, String> m_questions;
	private T m_defaultOption;
	private Map<String, T> m_answers;

	public String getId() {
		return m_id;
	}

	public void setId(String id) {
		m_id = id;
	}

	public String getType() {
		return m_type;
	}

	public void setType(String type) {
		m_type = type;
	}

	public String getDescription() {
		return m_description;
	}

	public void setDescription(String description) {
		m_description = description;
	}

	public T getDefaultValue() {
		return m_defaultValue;
	}

	public void setDefaultValue(T defaultValue) {
		m_defaultValue = defaultValue;
	}

	public Map<String, String> getQuestions() {
		return m_questions;
	}

	public void setQuestions(Map<String, String> questions) {
		m_questions = questions;
	}

	public T getDefaultOption() {
		return m_defaultOption;
	}

	public void setDefaultOption(T defaultOption) {
		m_defaultOption = defaultOption;
	}

	public Map<String, T> getAnswers() {
		return m_answers;
	}

	public void setAnswers(Map<String, T> answers) {
		m_answers = answers;
	}

	@Override
	public String toString() {
		return "PromptMultiQuestion [m_id=" + m_id + ", m_type=" + m_type + ", m_description=" + m_description
				+ ", m_defaultValue=" + m_defaultValue + ", m_questions=" + m_questions + ", m_defaultOption="
				+ m_defaultOption + ", m_answers=" + m_answers + "]";
	}
}
