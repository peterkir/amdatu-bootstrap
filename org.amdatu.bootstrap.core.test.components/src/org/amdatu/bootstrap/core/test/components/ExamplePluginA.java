/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.test.components;

import java.io.File;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.DynamicSelect;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.ProjectBndFile;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Select;
import org.amdatu.bootstrap.command.SelectOption;
import org.amdatu.bootstrap.command.WorkspaceBundleSelect;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.Prompt;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class ExamplePluginA implements BootstrapPlugin{

	@Command
	public String toUpperCommand(String t) {
		return t.toUpperCase();
	}
	
	interface ExampleArgs extends Parameters{
		String withoutDescription();
		
		@Description("Example arg")
		String withDescription();		
		
		@Required
		String required();
		
		File normalFile();
		
		@RunConfig
		File bndRunFile();
		
		@Select({
			@SelectOption(value="A", description="Option A"),
			@SelectOption(value="B", description="Option B"),
			@SelectOption(value="C", description="Option C")})
		String staticStringOptions();
		
		@DynamicSelect("getListOfOptions")
		String dynamicSelect();
		
		@WorkspaceBundleSelect
		String bundle();
		
		@ProjectBndFile
		String bndFile();
	}
	
	@Command
	public String withInterface(ExampleArgs args)  {
		return "";
	}
	
	@Command
	public String withPrompt(Prompt prompt) {
		boolean askBoolean = prompt.askBoolean("Enable", false);
		return "The answer was " + askBoolean;
	}
	
	@Command
	public void voidCommand(Prompt prompt) {
		prompt.println("Fire and forget!");
	}
	
	@Command
	public void errorCommand() {
		throw new RuntimeException("Something went wrong!");
	}

	
	
	@Override
	public String getName() {
		return "example";
	}

}
